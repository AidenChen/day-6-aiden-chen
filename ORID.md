## Objective
- Code Review.
- Show team's Design Pattern.
- Refactoring & Practice.Learned about some ways to find code smell in our project and basic step for
  refactoring.

## Reflective
Good!


## Interpretive
- I learned new design patterns while presenting the design patterns prepared by the group.
- Learned about the methodological steps of refactoring, which I have not touched before.


## Decisional
- There are still many unfamiliar design patterns that require further study.
- Try applying the learning refactoring today to previous projects.

