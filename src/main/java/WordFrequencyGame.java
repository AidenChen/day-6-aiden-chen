import java.util.*;
import java.util.stream.Collectors;


public class WordFrequencyGame {

    public static final String REGEX_SPLIT = "\\s+";

    public static final String JOIN_DELIMITER = "\n";

    public String getResult(String inputStr){
        try {
            String[] splitWords = inputStr.split(REGEX_SPLIT);
            Map<String, Long> wordCountMap = countWords(splitWords);
            List<Input> inputList = wordCountMapToSortedInputsDesc(wordCountMap);
            return transformInputListToStr(inputList);
        } catch (Exception e) {
            return "Calculate Error";
        }
    }

    private Map<String, Long> countWords(String[] strArray) {
        return Arrays.stream(strArray).collect(Collectors.groupingBy(String::valueOf, Collectors.counting()));
    }

    private List<Input> wordCountMapToSortedInputsDesc(Map<String, Long> strCountMap) {
        return strCountMap
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .map(entry -> new Input(entry.getKey(), Math.toIntExact(entry.getValue())))
                .collect(Collectors.toList());
    }

    private String transformInputListToStr(List<Input> inputList) {
        return inputList
                .stream()
                .map(input -> String.format("%s %d", input.getValue(), input.getWordCount()))
                .collect(Collectors.joining(JOIN_DELIMITER));
    }


}
